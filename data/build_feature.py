import requests
from web3 import Web3, HTTPProvider, IPCProvider
import sys
from .etherscan_internal_tx import get_internal_txs_for_account

web3 = Web3(HTTPProvider('https://mainnet.infura.io:8545'))

def parse_eth_txs(account_address):
    url = "http://api.etherscan.io/api?module=account&action=txlist&address={}&startblock=0&endblock=99999999&sort=asc&apikey=YourApiKeyToken".format(account_address)
    response = requests.get(url)
    data = response.json()
    results = data["result"]
    txs = []
    for result in results:
        if result["isError"] == "0":
            value = web3.fromWei(int(result["value"]),"ether")
            from_a = result["from"]
            to_a = result["to"]
            if to_a == "":
                to_a = result["contractAddress"] + " contractAddress"
            txs.append([from_a,to_a,value])
    return txs

def build_receipt_amount(txs,is_frequency):
    receipt_amount_out = dict()
    receipt_amount_in = dict()

    for tx in txs:
        from_address,to_address,quantity = tx
        if is_frequency:
            quantity = 1
        if to_address not in receipt_amount_out:
            receipt_amount_out[to_address] = quantity
        else:
            receipt_amount_out[to_address] += quantity

        if from_address not in receipt_amount_in:
            receipt_amount_in[from_address] = quantity
        else:
            receipt_amount_in[from_address] += quantity
    return (receipt_amount_out,receipt_amount_in)

def build_clustering_feature_for_an_account(account_address):
    account_address = account_address.lower()
    txs = parse_eth_txs(account_address) + get_internal_txs_for_account(account_address)

    eth_amount_receipt_amount_out, eth_amount_receipt_amount_in = build_receipt_amount(txs,False)
    freq_amount_receipt_amount_out, freq_amount_receipt_amount_in = build_receipt_amount(txs,True)

    if account_address in eth_amount_receipt_amount_out:
        total_eth_sent = eth_amount_receipt_amount_out[account_address]
    else:
        total_eth_sent = 0

    if account_address in eth_amount_receipt_amount_in:
        total_eth_received = eth_amount_receipt_amount_in[account_address]
    else:
        total_eth_received = 0

    if account_address in freq_amount_receipt_amount_out:
        total_send_txs_count = freq_amount_receipt_amount_out[account_address]
    else:
        total_send_txs_count = 0

    if account_address in freq_amount_receipt_amount_in:
        total_received_txs_count = freq_amount_receipt_amount_in[account_address]
    else:
        total_received_txs_count = 0

    return (float(total_eth_sent),float(total_eth_sent)-float(total_eth_received),total_send_txs_count,total_send_txs_count-total_received_txs_count)

if __name__ == "__main__":
    build_clustering_feature_for_an_account(sys.argv[1])
