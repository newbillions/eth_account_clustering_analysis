import requests
from datetime import datetime
import pandas as pd
from .html_helper import find_tx_given_token_contract_address
import numpy as np
from .build_feature import build_clustering_feature_for_an_account
import numpy as np

Watch_addr = "0x255aa6df07540cb5d3d297f0d0d4d84cb52bc8e6"    #Radien Network
#BUILD_FEATURE : etherdelta,binance,liqui.io, RaidenMultiSigWallet,big whale
escape_accounts = ["0x8d12a197cb00d4747a1fe03395095ce2a5cc6819",'0x3f5ce5fbfe3e9af3971dd833d26ba9b5c936f0be','0x8271b2e8cbe29396e9563229030c89679b9470db','0x00c7122633a4ef0bc72f7d02456ee2b11e97561e','0x198ef1ec325a96cc354c7266a038be8b5c558f67']

def addHolder(addr,value):          #Add holders to dict
    if holders.get(addr):              #if dict is not empty
        holders[addr] += value
    else:
        holders[addr] = value

def find_interstering_accounts():
    # Find accounts
    txs = find_tx_given_token_contract_address(Watch_addr,1,1)
    return txs

def normalize_acc_features(acc_features):
    features = list(acc_features.values())

    f_pd = pd.DataFrame(features)
    print(f_pd)
    eth_in_sum = f_pd.loc[:,0].mean()
    eth_in_std = f_pd.loc[:,0].std()
    f_pd.loc[:,0] = 10*(f_pd.loc[:,0] - eth_in_sum) / eth_in_std
    print(f_pd)

    eth_out_sum = f_pd.loc[:,1].mean()
    eth_out_std = f_pd.loc[:,1].std()
    f_pd.loc[:,1] = 10*(f_pd.loc[:,1] - eth_in_sum) / eth_in_std
    print(f_pd)

    total_send_txs_count_sum = f_pd.loc[:,2].mean()
    total_send_txs_count_std = f_pd.loc[:,2].std()
    f_pd.loc[:,2] = 10*(f_pd.loc[:,2] - total_send_txs_count_sum) / total_send_txs_count_std

    total_received_txs_count_sum = f_pd.loc[:,3].mean()
    total_received_txs_count_std = f_pd.loc[:,3].std()
    f_pd.loc[:,3] = 10*(f_pd.loc[:,3] - total_received_txs_count_sum) / total_received_txs_count_std

    new_acc_features = dict()
    for index,acc in enumerate(acc_features):
        new_acc_features[acc] = f_pd.iloc[index]
    return new_acc_features

def build_feature_from_account_arr(all_arr):
    # Build feature for those accounts
    acc_features = dict()
    print("len(all_arr): {}".format(len(all_arr)))
    counter = 0
    for account in all_arr:
        counter += 1
        print("{}/{} {}".format(counter,len(all_arr),account))
        if account in escape_accounts:
            continue
        acc_features[account] = build_clustering_feature_for_an_account(account)
    # acc_features = normalize_acc_features(acc_features)
    return acc_features

def main():
    all_arr = find_interstering_accounts()
    acc_features = build_feature_from_account_arr(all_arr)
    return acc_features

if __name__ == "__main__":
    main()
